import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/tabbar/root_page.dart';
// 沉浸式状态栏
import 'package:flutter/services.dart';
import 'dart:io';

void main() {
  // 沉浸式状态栏

  runApp(const APP());
}

class APP extends StatelessWidget {
  const APP({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(Platform.isAndroid){
      SystemUiOverlayStyle systemUiOverlayStyle =
      SystemUiOverlayStyle(statusBarColor: Colors.transparent);
      SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // 去掉水波纹
        highlightColor:const Color.fromRGBO(0, 0, 0, 0),
      ),
      title: "微信", // 安卓进入后台显示的名称
      home: RootPage(),
    );
  }
}

