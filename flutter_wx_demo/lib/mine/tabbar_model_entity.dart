// To parse this JSON data, do
//
//     final tabbarModel = tabbarModelFromMap(jsonString);

import 'dart:convert';

TabbarModel tabbarModelFromMap(String str) =>
    TabbarModel.fromMap(json.decode(str));

String tabbarModelToMap(TabbarModel data) => json.encode(data.toMap());

class TabbarModel {
  TabbarModel({
    required this.list,
  });

  final List<ListElement> list;

  factory TabbarModel.fromMap(Map<String, dynamic> json) => TabbarModel(
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "list": List<dynamic>.from(list.map((x) => x.toMap())),
      };
}

class ListElement {
  ListElement({
    this.id,
    this.postTitle,
    this.postThemeType,
    this.postPublishedType,
    this.imgType,
    this.more,
    this.videoUrl,
    this.webUrl,
    this.userNickname,
    this.columnId,
    this.postExcerpt,
    this.userId,
    this.thumbnail,
    this.backgroundCover,
    this.videoTime,
    // this.videoDefinition,
    this.columnData,
    this.author,
  });

  final int? id;
  final String? postTitle;
  final int? postThemeType;
  final int? postPublishedType;
  final int? imgType;
  final String? more;
  final String? videoUrl;
  final String? webUrl;
  final String? userNickname;
  final int? columnId;
  final String? postExcerpt;
  final int? userId;
  final List<String>? thumbnail;
  final String? backgroundCover;
  final String? videoTime;

  // final VideoDefinition videoDefinition;
  final ColumnData? columnData;
  final Author? author;

  factory ListElement.fromMap(Map<String, dynamic> json) => ListElement(
        id: json["id"],
        postTitle: json["post_title"],
        postThemeType: json["post_theme_type"],
        postPublishedType: json["post_published_type"],
        imgType: json["img_type"],
        more: json["more"],
        videoUrl: json["video_url"],
        webUrl: json["web_url"],
        userNickname: json["user_nickname"],
        columnId: json["column_id"],
        postExcerpt: json["post_excerpt"],
        userId: json["user_id"],
        thumbnail: List<String>.from(json["thumbnail"].map((x) => x)),
        backgroundCover: json["background_cover"],
        videoTime: json["video_time"],
        // videoDefinition: VideoDefinition.fromMap(json["video_definition"]),
        columnData: json["column_data"] == null
            ? null
            : ColumnData.fromMap(json["column_data"]),
        author: Author.fromMap(json["author"]),
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "post_title": postTitle,
        "post_theme_type": postThemeType,
        "post_published_type": postPublishedType,
        "img_type": imgType,
        "more": more,
        "video_url": videoUrl,
        "web_url": webUrl,
        "user_nickname": userNickname,
        "column_id": columnId,
        "post_excerpt": postExcerpt,
        "user_id": userId,
        // "thumbnail": List<dynamic>.from(thumbnail.map((x) => x)),
        "background_cover": backgroundCover,
        "video_time": videoTime,
        // "video_definition": videoDefinition.toMap(),
        "column_data": columnData == null ? ColumnData() : columnData!.toMap(),
        "author": author == null ? null : author!.toMap(),
      };
}

class Author {
  Author({
    this.id,
    this.avatar,
    this.userNickname,
    this.signature,
    this.auth,
    this.authTemplate,
    this.postNum,
    this.wireNum,
    this.videoNum,
    this.authName,
    this.authData,
    this.followStatus,
  });

  final int? id;
  final String? avatar;
  final String? userNickname;
  final String? signature;
  final int? auth;
  final String? authTemplate;
  final int? postNum;
  final int? wireNum;
  final int? videoNum;
  final String? authName;
  final List<dynamic>? authData;
  final int? followStatus;

  factory Author.fromMap(Map<String, dynamic> json) => Author(
        id: json["id"],
        avatar: json["avatar"],
        userNickname: json["user_nickname"],
        signature: json["signature"],
        auth: json["auth"],
        authTemplate: json["auth_template"],
        postNum: json["post_num"],
        wireNum: json["wire_num"],
        videoNum: json["video_num"],
        authName: json["auth_name"],
        authData: json["auth_data"] == null
            ? json["auth_data"]
            : List<dynamic>.from(json["auth_data"].map((x) => x)),
        followStatus: json["follow_status"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "avatar": avatar,
        "user_nickname": userNickname,
        "signature": signature,
        "auth": auth,
        "auth_template": authTemplate,
        "post_num": postNum,
        "wire_num": wireNum,
        "video_num": videoNum,
        "auth_name": authName,
        "auth_data": authData == null
            ? null
            : List<dynamic>.from(authData!.map((x) => x)),
        "follow_status": followStatus,
      };
}

class ColumnData {
  ColumnData({
    this.id,
    this.name,
    this.columnColor,
    this.description,
    this.followStatus,
    this.thumbnail,
  });

  final int? id;
  final String? name;
  final String? columnColor;
  final String? description;
  final int? followStatus;
  final String? thumbnail;

  factory ColumnData.fromMap(Map<String, dynamic> json) => ColumnData(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        columnColor: json["column_color"] == null ? null : json["column_color"],
        description: json["description"] == null ? null : json["description"],
        followStatus:
            json["follow_status"] == null ? null : json["follow_status"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? 0 : id,
        "name": name == null ? "" : name,
        "column_color": columnColor == null ? "" : columnColor,
        "description": description == null ? "" : description,
        "follow_status": followStatus == null ? 0 : followStatus,
        "thumbnail": thumbnail == null ? "" : thumbnail,
      };
}
