import 'dart:math';

import 'package:flutter/material.dart';

class KeyDemo extends StatefulWidget {
  const KeyDemo({Key? key}) : super(key: key);

  @override
  _KeyDemoState createState() => _KeyDemoState();
}
// 可以尝试删除key 看看效果
//localkey
/*
*
* globalkey 作用和tag 相似 需要写demo
* localkey
* 1.valuekey 用值来作为参数
* 2.objectKey 用对象来作为参数
* 3.Uniquekey 唯一标识？？需要自己玩一下
* */
class _KeyDemoState extends State<KeyDemo> {
  List<Widget> _list = [KeyItem(title: "1",key: ValueKey(0),),KeyItem(title: "2",key: ValueKey(1)),KeyItem(title: "3",key: ValueKey(2))];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("keydemo"),
        centerTitle: true,
      ),
      body: Row(
        children: _list,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          _list.length==0?_list.add(KeyItem(title: "1",key: ValueKey(_list.length),)):_list.removeAt(0);
          setState(() {

          });
        },
      ),
    );
  }
}

class KeyItem extends StatefulWidget {
  final String title;
  const KeyItem({required this.title,Key? key}) : super(key: key);
  @override
  _KeyItemState createState() => _KeyItemState();
}

class _KeyItemState extends State<KeyItem> {
  Color _color = Color.fromRGBO(Random().nextInt(255), Random().nextInt(255), Random().nextInt(255), 1);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      color: _color,
      child: Text(widget.title),
    );
  }
}

