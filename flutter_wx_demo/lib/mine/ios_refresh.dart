import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/mine/banner_widget.dart';

class IosRefreshPage extends StatefulWidget {
  const IosRefreshPage({Key? key}) : super(key: key);

  @override
  _IosRefreshPageState createState() => _IosRefreshPageState();
}

class _IosRefreshPageState extends State<IosRefreshPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ios Refresh"),
        centerTitle: true,
      ),
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          CupertinoSliverRefreshControl(
            // refreshIndicatorExtent: 60,/// 刷新过程中悬浮高度
            // refreshTriggerPullDistance: 100,///触发刷新的距离
            onRefresh: () async {
              //模拟网络请求
              await Future.delayed(Duration(milliseconds: 1000));
              //结束刷新
              return Future.value(true);
            },
          ),
          SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
            return Container(
              color: Colors.white,
              height: 44,
              child: BannerWidget(
                  list: ["images/公众号.png", "images/微信设置.png", "images/小程序.png"],
                  curttentIndex: 0,
                  loopDuring: 1),
            );
          }, childCount: 1))
        ],
      ),
    );
  }

  SliverAppBar buildSliverAppBar() {
    return SliverAppBar(
      title: Text("ios Refresh"),
      centerTitle: true,
    );
  }
}
