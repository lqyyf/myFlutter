import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/http_manager.dart';
import 'package:flutter_wx_demo/common/root_tabbar.dart';
import 'package:flutter_wx_demo/mine/tabChange/tab_live.dart';
import 'package:flutter_wx_demo/mine/tabChange/tab_tread.dart';
import 'package:flutter_wx_demo/mine/tabbar_model_entity.dart';

class TabbarChange extends StatefulWidget {
  const TabbarChange({Key? key}) : super(key: key);

  @override
  _TabbarChangeState createState() => _TabbarChangeState();
}

class _TabbarChangeState extends State<TabbarChange>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;
  List<String> _list = ["圆桌", "推荐", "视频", "直播"];

  @override
  void initState() {
    // TODO: implement initState
    _tabController = TabController(length: _list.length, vsync: this);
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    if(_tabController != null){
      _tabController!.dispose();
    }
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Container(
              // height: 44,
              child: titleLayout(),
            ),
          ),
          body: bodyLayout(),
        ));
  }

  Widget bodyLayout() {
    return TabBarView(
        controller: _tabController,
        physics:BouncingScrollPhysics(),
        children: _list.map((e) {
          if(e == "推荐"){
            return Tread();
          }
          return TabLive();
        }).toList());
  }

  Widget titleLayout() {
    return TabBar(
      physics: const BouncingScrollPhysics(),
      controller: _tabController,
      tabs: _list
          .map((e) => Tab(
                text: e,
              ))
          .toList(),
      indicatorColor: Color.fromRGBO(255, 85, 35, 1),
      isScrollable: true,
      labelPadding: EdgeInsets.only(left: 10, right: 10, bottom: 3),
      labelColor: Color.fromRGBO(34, 26, 24, 1),
      labelStyle: TextStyle(fontSize: 21, fontWeight: FontWeight.normal),
      unselectedLabelColor: Color.fromRGBO(34, 26, 24, 0.4),
      unselectedLabelStyle:
          TextStyle(fontSize: 17, fontWeight: FontWeight.normal),
      indicator: BoxDecoration(),
      dragStartBehavior: DragStartBehavior.start,
      indicatorSize: TabBarIndicatorSize.label,
    );
  }
}
