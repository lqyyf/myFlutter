import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';
import 'package:flutter_wx_demo/found/found_cell.dart';
import 'package:flutter_wx_demo/found/found_child.dart';
import 'package:flutter_wx_demo/mine/data_share.dart';
import 'package:flutter_wx_demo/mine/ios_refresh.dart';
import 'package:flutter_wx_demo/mine/key_demo.dart';
import 'package:flutter_wx_demo/mine/tabbar_change.dart';
import 'package:image_picker/image_picker.dart';

class MinePage extends StatefulWidget {
  const MinePage({Key? key}) : super(key: key);

  @override
  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  final ImagePicker picker = new ImagePicker();
  void _pickImg() async {
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    print(image?.path ?? "没有内容");
  }

  Widget headerWidget() {
    return Container(
        color: Colors.white,
        height: 200,
        child: Container(
            margin: EdgeInsets.only(top: 100, left: 10, right: 10, bottom: 10),
            child: Row(
              children: [
                GestureDetector(
                  onTap: (){
                    _pickImg();
                  },
                  child: Container(
                    width: 70,
                    height: 70,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      child: Image.network(
                          "https://randomuser.me/api/portraits/men/77.jpg"),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width - 105,
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("cdma"),
                      Text("微信号：10086"),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 75),
                  color: Colors.white,
                  child: Image.asset(
                    "images/icon_right.png",
                    width: 15,
                  ),
                )
              ],
            )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: RootColors.nav_back_color,
      //   elevation: 0,
      //   title: const Text(
      //     "我的",
      //     style: TextStyle(
      //         color: RootColors.nav_titile_color,
      //         fontSize: 20,
      //         fontWeight: FontWeight.bold),
      //   ),
      //   centerTitle: true,
      // ),
        body: Container(
            color: RootColors.nav_back_color,
            child: Stack(
              children: [
                Container(
                  child: MediaQuery.removePadding(
                      removeTop: true,
                      context: context,
                      child: ListView(
                        physics: BouncingScrollPhysics(),
                        children: [
                          this.headerWidget(),
                          SizedBox(
                            height: 10,
                          ),
                          FoundCell(
                            t: "支付",
                            imgName: "images/微信 支付.png",
                            clickBack: (s) {
                              print("点击支付$s");
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      FoundChild("${s}")));
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          FoundCell(
                            t: "ios 风格的刷新页面",
                            imgName: "images/微信收藏.png",
                            clickBack: (s) {
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: (
                                      BuildContext context) => IosRefreshPage()
                                  ));
                            },
                          ),
                          FoundCell(
                            t: "key的使用",
                            imgName: "images/朋友圈.png",
                            clickBack: (s) {
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: (
                                      BuildContext context) => KeyDemo()
                                  ));
                            },
                          ),
                          FoundCell(
                            t: "数据共享",
                            imgName: "images/微信卡包.png",
                            clickBack: (s) {
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: (
                                      BuildContext context) => DataShare()
                                  ));
                            },
                          ),
                          FoundCell(
                            t: "tabbar切换",
                            imgName: "images/微信卡包.png",
                            clickBack: (s){
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: (
                                      BuildContext context) => TabbarChange()
                                  ));
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          FoundCell(
                            t: "表情",
                            imgName: "images/微信表情.png",
                          ),
                          FoundCell(
                            t: "设置",
                            imgName: "images/微信设置.png",
                          ),
                        ],
                      )),
                ),
                Positioned(
                    right: 10,
                    top: 44,
                    child: Image.asset(
                      "images/相机.png",
                      width: 20,
                      height: 20,
                    ))
              ],
            )));
  }

}
