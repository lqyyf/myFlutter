import 'package:flutter/material.dart';

class Tread extends StatefulWidget {
  const Tread({Key? key}) : super(key: key);

  @override
  _TreadState createState() => _TreadState();
}

class _TreadState extends State<Tread> with AutomaticKeepAliveClientMixin {
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: itemBuilderCell,
      itemCount: 100,
      physics: BouncingScrollPhysics(),
    );
  }

  Widget itemBuilderCell(BuildContext context, int index) {
    return Container(
      color: Colors.red,
      margin: EdgeInsets.only(bottom: 10),
      height: 100,
    );
  }
}
