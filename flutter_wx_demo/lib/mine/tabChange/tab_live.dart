import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_common_export.dart';
import 'package:flutter_wx_demo/mine/tabbar_model_entity.dart';

class TabLive extends StatefulWidget {
  const TabLive({Key? key}) : super(key: key);

  @override
  _TabLiveState createState() => _TabLiveState();
}

class _TabLiveState extends State<TabLive> with AutomaticKeepAliveClientMixin {
  TabbarModel _model = TabbarModel(list: []);

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("111");
    reuqest();
  }

  void reuqest() async {
    final r = await HttpManager().get("https://api.dev.pingwest.com/v3/article/v2/video-list?last_id=0",
        {});
    print(r);

    final res = await HttpManager().get(
        "https://api.dev.pingwest.com/v3/article/v2/video-list?last_id=0", {});
    // final str = json.encode(res.data["data"]["list"]);
    _model = TabbarModel.fromMap(res.data["data"]);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: itemCell,
      itemCount: _model.list.length,
      physics: BouncingScrollPhysics(),
    );
  }

  Widget itemCell(BuildContext context, int index) {
    ListElement element = _model.list[index];
    return Container(
      margin: EdgeInsets.only(top: 8, bottom: 8),
      decoration:const BoxDecoration(
          color: RootColors.root_common_bgcolor,
          borderRadius: BorderRadius.all(Radius.circular(16))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          /*图片*/
          Container(
              child: Stack(
            children: [
              Container(
                  width: RootFrame.screenWidth(context),
                  margin: const EdgeInsets.only(
                      left: 16, top: 16, right: 16, bottom: 12),
                  height: 200,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Image.network(
                      "${element.backgroundCover}",
                      fit: BoxFit.cover,
                    ),
                  )),
              Positioned(
                  right: 30, bottom: 20, child: Text("${element.videoTime}"))
            ],
          )),
          Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              width: RootFrame.screenWidth(context) - 32,
              child: Text(
                "${element.postTitle}",
                textAlign: TextAlign.left,
                style:const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              )),
          Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              width: RootFrame.screenWidth(context) - 32,
              child: Text(
                "使用的DecorationImage，相当于把图片当做一个背景，这里需要注意的就是Container的child的尺寸问题，就算不放内容，也需要设置一个带尺寸的child Widget",
                textAlign: TextAlign.left,
              )),
          Container(
            margin: EdgeInsets.only(left: 16, top: 8, right: 16, bottom: 16),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  child: Image.network(
                    element.author == null
                        ? "${element.columnData!.thumbnail}"
                        : "${element.author!.avatar}",
                    width: 40,
                    height: 40,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(element.author == null
                    ? "${element.columnData!.name}"
                    : "${element.author!.userNickname}"),
              ],
            ),
          )
        ],
      ),
    );
  }
}
