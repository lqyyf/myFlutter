import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_common_export.dart';

class BannerWidget extends StatefulWidget {
  final List<String> list;
  final int curttentIndex;
  final int loopDuring;

  const BannerWidget(
      {required this.list, required this.curttentIndex,required this.loopDuring});

  @override
  _BannerWidgetState createState() => _BannerWidgetState();
}

class _BannerWidgetState extends State<BannerWidget> {
  PageController? _pageController;
  Timer?_timer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("initState");
    _pageController = PageController(initialPage: widget.curttentIndex);
    startTimer();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    stopTimer();
    super.dispose();
  }
  void startTimer() {
    _timer = Timer(Duration(seconds: widget.loopDuring), () {
      print("startTimer");
      _pageController!.nextPage(duration: Duration(milliseconds: 200), curve: Curves.easeIn);
    });
  }
  void stopTimer(){
    if(_timer !=null && _timer!.isActive){
      _timer!.cancel();
    }
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: _buildContain(),
    );
  }

  _buildContain() {
    return PageView.builder(
      controller: _pageController,
        onPageChanged: (value) {
          // print("${value}");
        },
        itemCount: widget.list.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 200,
            color: Colors.red,
            width: RootFrame.screenWidth(context),
            child: Image.asset(widget.list[index]),
          );
        });
  }
}
