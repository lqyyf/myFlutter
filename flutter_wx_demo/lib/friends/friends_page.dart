
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_common_export.dart';

class _FriendModel {
  final String? imageUrl;
  final String? name;
  final String? indexTitle;
  final String? imageAsset;

  _FriendModel({
    this.imageUrl,
    @required this.name,
    @required this.indexTitle,
    this.imageAsset,
  });
}

class _FriendsCell extends StatelessWidget {
  final _FriendModel model;
  final bool? isShow;

  _FriendsCell({required this.model, required this.isShow});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Container(
            height: isShow == false ? 0 : 20,
            color: RootColors.chat_page_cellline_color,
            width: double.infinity,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Text("${model.indexTitle}"),
          ),
          Row(
            children: [
              Container(
                width: 40,
                height: 40,
                margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                    child: model.imageUrl == null
                        ? Image.asset(
                            "${model.imageAsset}",
                            width: 50,
                            height: 50,
                          )
                        : Image.network("${model.imageUrl}",
                            width: 50, height: 50)),
              ),
              Expanded(
                  child: Container(
                height: 56,
                alignment: Alignment.centerLeft,
                decoration: const BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                  color: RootColors.chat_page_cellline_color,
                  width: 0.5,
                ))),
                child: Text(
                  "${model.name}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ))
            ],
          )
        ],
      ),
    );
  }
}

class FriendsPage extends StatefulWidget {
  const FriendsPage({Key? key}) : super(key: key);

  @override
  _FriendsPageState createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> {
  bool _selSlider = false;
  final List<String> _listSlider = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I'
  ];
  final List<Widget> _listWidget = [];

  final List<_FriendModel> _listData = [
    _FriendModel(
      name: "新的朋友",
      indexTitle: null,
      imageAsset: "images/新的朋友.png",
    ),
    _FriendModel(
      name: "群聊",
      indexTitle: null,
      imageAsset: "images/群聊.png",
    ),
    _FriendModel(
      name: "标签",
      indexTitle: null,
      imageAsset: "images/群聊.png",
    ),
    _FriendModel(
      name: "公众号",
      indexTitle: null,
      imageAsset: "images/公众号.png",
    ),
  ];
  final List<_FriendModel> _listUser = [
    _FriendModel(
      name: "A哈哈",
      indexTitle: "A",
      imageUrl: "https://randomuser.me/api/portraits/men/75.jpg",
    ),
    _FriendModel(
      name: "B哈哈",
      indexTitle: "B",
      imageUrl: "https://randomuser.me/api/portraits/men/76.jpg",
    ),
    _FriendModel(
      name: "C哈哈",
      indexTitle: "C",
      imageUrl: "https://randomuser.me/api/portraits/men/77.jpg",
    ),
    _FriendModel(
      name: "A哈哈A1",
      indexTitle: "A",
      imageUrl: "https://randomuser.me/api/portraits/men/77.jpg",
    ),
    _FriendModel(
      name: "A哈哈A2",
      indexTitle: "A",
      imageUrl: "https://randomuser.me/api/portraits/men/77.jpg",
    ),
    _FriendModel(
      name: "B哈哈B1",
      indexTitle: "B",
      imageUrl: "https://randomuser.me/api/portraits/men/77.jpg",
    ),
    _FriendModel(
      name: "D哈哈D1",
      indexTitle: "D",
      imageUrl: "https://randomuser.me/api/portraits/women/77.jpg",
    ),
  ];
  final List<_FriendModel> _user = [];
  ScrollController? _scrollController;

  Widget _itemForRowBulider(BuildContext context, int index) {
    if (index == 0) {
      return Container(
        child: _FriendsCell(model: _user[index], isShow: false),
      );
    }
    return Container(
      child: _FriendsCell(
          model: _user[index],
          isShow: _user[index].indexTitle == _user[index - 1].indexTitle
              ? false
              : true),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: RootColors.nav_back_color,
        elevation: 0,
        title: const Text(
          "通讯录",
          style: TextStyle(
              color: RootColors.nav_titile_color,
              fontSize: 20,
              fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: [
          Positioned(
              child: Container(
            width: double.infinity,
            child: ListView.builder(
              controller: _scrollController,
              itemBuilder: _itemForRowBulider,
              itemCount: _user.length,
            ),
          )),
          Positioned(
              top: 50,
              right: 0,
              bottom: 50,
              child: GestureDetector(
                onHorizontalDragDown: (DragDownDetails details){
                  print("爷拖拽了");
                  setState(() {
                    _selSlider=true;
                  });
                },
                onHorizontalDragEnd: (DragEndDetails details){
                  print("爷拖拽了结束了");
                  setState(() {
                    _selSlider=false;
                  });
                },
                child: Container(
                    color: _selSlider ? Colors.red : Color.fromRGBO(0, 0, 0, 0),
                    width: 50,
                    height: RootFrame.screenHeight(context) - 100,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: _listWidget)),
              )
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController = ScrollController();
    _user
      ..addAll(_listUser)
      ..addAll(_listUser)
      ..addAll(_listUser)
      ..addAll(_listUser);
    _user.sort((_FriendModel a, _FriendModel b) {
      return a.indexTitle!.compareTo(b.indexTitle!);
    });
    _user.insertAll(0, _listData);

    for (var i = 0; i < _listSlider.length; i++) {
      _listWidget.add(Text(
        "${_listSlider[i]}",
        style: const TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      ));
    }
  }
}
