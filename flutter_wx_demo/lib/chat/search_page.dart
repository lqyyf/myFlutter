import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_wx_demo/chat/search_bar.dart';
import 'package:flutter_wx_demo/chat/search_cell.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';
import 'package:flutter_wx_demo/common/root_common_export.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(length: 1, child: buildNestedScrollView());
  }

  NestedScrollView buildNestedScrollView() {
    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool b) {
        return [_buildSliverAppBar()];
      },

      ///主体部分
      body: TabBarView(controller: _tabController, children: [
        Container(
          height: 1000,
          color: Colors.red,
          child: Text("data"),
        ),
      ]),
    );
  }

  SliverAppBar _buildSliverAppBar() {
    return SliverAppBar(
      snap: false,
      floating: true,
      pinned: true,
      expandedHeight: 88,
      backgroundColor: Colors.grey,
      elevation: 0.0,
      leading: Text(""),
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        // collapseMode: CollapseMode.parallax,
        title: Container(
            // child: Text("3456789087654567890876543567890876543567890"),
            ),
        titlePadding: EdgeInsetsDirectional.all(0),
        background: Container(
          child: FlutterLogo(),
        ),
      ),
      bottom: TabBar( 
        controller: _tabController,
        padding: EdgeInsets.all(0),
        tabs: [
          Container(
            color: Colors.amberAccent,
            height: 34,
            child: Row(
              children: [
                Expanded(child:  Container(
                  color: Colors.black,
                ),),
                Text("取消")
              ],
            ),
          ),
        
        ],
        // EdgeInsetsGeometry
        indicatorSize: TabBarIndicatorSize.label,
        indicatorColor: Colors.black,
        indicatorWeight: 2,
      ),
    );
  }

  Widget _list() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Container(
            height: 44,
            child: Text("第 $index 个"),
          );
        },
        childCount: 100,
      ),
    );
  }
}
