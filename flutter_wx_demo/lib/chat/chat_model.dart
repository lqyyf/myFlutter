import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';

class ChatCard extends StatelessWidget {
  // const ChatCard({Key? key}) : super(key: key);
  final ChaModel model;
  ChatCard({required this.model});
  Widget leftWidget() {
    return Row(
      children: [this.imgWiaget(), Expanded(child: this.infoWidget())],
    );
  }

  Widget imgWiaget() {
    return Stack(
      children: [
        Container(
          width: 76,
          height: 85,
        ),
        Positioned(
          top: 10,
          left: 15,
          child: Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                image: DecorationImage(image: NetworkImage("${model.userImg}"))),
          ),
        ),
        Positioned(
            right: 0,
            top: 5,
            child: ClipOval(
              child: Container(
                width: 10,
                height: 10,
                color: Colors.red,
              ),
            ))
      ],
    );
  }

  Widget infoWidget() {
    return Container(
        height: 85,
        margin: const EdgeInsets.only(left: 10),
        padding: const EdgeInsets.only(top: 20, right: 30),
        decoration: const BoxDecoration(
            border: Border(
                bottom: BorderSide(
              color: RootColors.chat_page_cellline_color,
              width: 1,
            ))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${model.name}",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            Padding(padding: EdgeInsets.only(top: 4)),
            Text(
              "${model.detail}",
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.normal),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ));
  }

  Widget timeWidget() {
    return Container(
      width: 60,
      height: 85,
      padding:const EdgeInsets.only(top: 17),
      decoration: const BoxDecoration(
          border: Border(
              bottom: BorderSide(
            color: RootColors.chat_page_cellline_color,
            width: 1,
          ))),
      child: Text("08:08"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // this.leftWidget(),
          Expanded(child: this.leftWidget()),
          this.timeWidget(),
        ],
      ),
    );
  }
}

class ChaModel {
  final String? userImg;
  final String? name;
  final String? detail;
  final String? time;

  const ChaModel({this.userImg, this.name, this.detail, this.time});
  factory ChaModel.fromMap(Map json){
      return ChaModel(
        name: json["user_name"],
        detail: json["user_des"],
        userImg: json["user_img"],

      );
  }
}
