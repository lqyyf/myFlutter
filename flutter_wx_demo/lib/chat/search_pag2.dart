import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_common_export.dart';

class SearchPage2 extends StatefulWidget {
  const SearchPage2({Key? key}) : super(key: key);

  @override
  _SearchPage2State createState() => _SearchPage2State();
}

class _SearchPage2State extends State<SearchPage2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SearchBar(_searchBarValueChange),
          Expanded(
              flex: 1,
              child: MediaQuery.removePadding(
                  removeTop: true, context: context, child: _buildListView()))
        ],
      ),
    );
  }

  void _searchBarValueChange(value) {
    print("接收到底" + value);
  }
  ListView _buildListView() {
    return ListView.builder(
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: Text("333"),
          );
        });
  }
}

class SearchBar extends StatefulWidget {
  final ValueChanged<String> onchanged;

  const SearchBar(this.onchanged);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final TextEditingController _controller = TextEditingController();
  bool _showClear = false;

  void _onChange(String text) {
    if (widget.onchanged != null) widget.onchanged(text);
    setState(() {
      _showClear = text.length > 0 ? true : false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: RootFrame.screenSafaTop(context) + 44,
      color: RootColors.nav_back_color,
      child: Column(
        children: [
          SizedBox(
            height: RootFrame.screenSafaTop(context),
          ),
          Expanded(
            flex: 1,
            child: _buildSearchTF(),
          )
        ],
      ),
    );
  }

  Container _buildSearchTF() {
    return Container(
      // color: Colors.red,
      child: Row(
        children: [
          Expanded(
              child: Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.only(left: 5, right: 5),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "images/放大镜b.png",
                  color: Colors.grey,
                  width: 20,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    color: Colors.white,
                    child: _buildtextField(),
                  ),
                ),
                if (_showClear)
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _controller.clear();
                        _onChange("");
                      });
                    },
                    child: Icon(
                      Icons.cancel,
                      color: Colors.grey,
                    ),
                  )
              ],
            ),
          )),
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              margin: EdgeInsets.only(right: 5),
              child: Text("取消"),
            ),
          )
        ],
      ),
    );
  }

  TextField _buildtextField() {
    return TextField(
      onChanged: _onChange,
      controller: _controller,
      autofocus: true,
      textAlign: TextAlign.left,
      cursorColor: Colors.green,
      maxLines: 1,
      decoration: InputDecoration(
        counter: null,
        //这里如果设置了边框为border.none 就会无法对齐
        contentPadding: EdgeInsets.only(left: 5, bottom: 0, top: 0),
        border: OutlineInputBorder(borderSide: BorderSide.none),
        hintText: "  搜索",
      ),
    );
  }
}
