import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/chat/search_pag2.dart';
import 'package:flutter_wx_demo/chat/search_page.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';

class SearchCard extends StatelessWidget {
  const SearchCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => SearchPage2()));
      },
      child: Container(
        height: 44,
        color: RootColors.nav_back_color,
        child: buildSearchContainer(),
      ),
    );
  }

  Container buildSearchContainer() {
    return Container(
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5))
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset("images/放大镜b.png", width: 15, color: Colors.grey,),
          Text("搜索", style: TextStyle(
              fontSize: 15,
              color: Colors.grey
          ),)
        ],
      ),
    );
  }
}
