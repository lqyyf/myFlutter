import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/chat/search_cell.dart';

class Searchbar extends StatelessWidget implements PreferredSizeWidget {
  // const Searchbar({Key? key}) : super(key: key);
  final String? placeHolder;
  Searchbar({required this.placeHolder});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SearchCard(),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize =>  Size.fromHeight(44.0);
}
