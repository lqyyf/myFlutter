import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/chat/chat_model.dart';
import 'package:flutter_wx_demo/chat/search_cell.dart';
import 'package:flutter_wx_demo/common/http_manager.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';
import 'package:http/http.dart' as http;

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage>
    with AutomaticKeepAliveClientMixin<ChatPage> {
  List<ChaModel> _listChatModel = [];

  Widget _cellForRow(BuildContext context, int index) {
    if(index==0){
      return SearchCard();
    }
    return ChatCard(model: _listChatModel[index-1]);
  }

  Widget _cutsomPopMemnItem(String img, String title) {
    return Row(
      children: [
        Image(
          image: AssetImage(img),
          width: 20,
        ),
        SizedBox(
          width: 10,
        ),
        Text(title)
      ],
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBodyContainer(),
    );
  }

  Container _buildBodyContainer() {
    return Container(
      color: RootColors.root_common_bgcolor,
      child: _listChatModel.length == 0
          ? Center(child: Text("加载中...."))
          : ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: _listChatModel.length,
              itemBuilder: _cellForRow,
            ),

    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: Text(
        "微信",
        style: TextStyle(
            color: RootColors.nav_titile_color,
            fontSize: 20,
            fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
      actions: [
        Container(
          margin: EdgeInsets.only(right: 10),
          child: PopupMenuButton(
              offset: Offset(0, 60),
              color: RootColors.chat_page_detailtitle_color,
              child: Image.asset(
                "images/圆加.png",
                width: 25,
              ),
              itemBuilder: (BuildContext context) {
                return <PopupMenuItem<String>>[
                  PopupMenuItem(
                      child: _cutsomPopMemnItem("images/发起群聊.png", "发起群聊")),
                  PopupMenuItem(
                      child: _cutsomPopMemnItem("images/发起群聊.png", "发起群聊")),
                  PopupMenuItem(
                      child: _cutsomPopMemnItem("images/发起群聊.png", "发起群聊")),
                ];
              }),
        )
      ],
      backgroundColor: RootColors.nav_back_color,
      elevation: 0,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    requestList().then((value) {
      // print("dddd+${value}");
      setState(() {
        _listChatModel = value;
      });
    }).catchError((error) {
      print(error);
    });
  }

  Future<List<ChaModel>> requestList() async {
    final url = Uri.parse(
        "http://rap2api.taobao.org/app/mock/293317/httpdemo/users/list");
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final chatmap = json.decode(response.body);
      List<ChaModel> chatlist = chatmap['data']
          .map<ChaModel>((item) => ChaModel.fromMap(item))
          .toList();
      return chatlist;
    } else {
      throw Exception('statusCode:${response.statusCode}');
    }
    // //  json 转 map
    //   final chatMap = {
    //       'name' : '1111',
    //   };
    //   final chatJson = json.encode(chatMap);
    //   print("map 转 json"+chatJson);
    // //  map 转json
    //   final newChat = json.decode(chatJson);
    //   print("json 转 map ${newChat}");
    //
  }
}
