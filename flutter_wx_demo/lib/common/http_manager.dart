import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

class HttpManager {
// 单利写法1
  static final HttpManager _manager = HttpManager._initnal();

  factory HttpManager() => _manager;

  HttpManager._initnal() {
    init();
  }

  late Dio _dio;

  init() {
    _dio = Dio();
  }

  get(String url, Map<String, dynamic> quary) async {
    return await _dio.get(url, queryParameters: quary);
  }

  post(String url, Map<String, dynamic> body) async{
    return await _dio.post(url, queryParameters: body);
  }
}
