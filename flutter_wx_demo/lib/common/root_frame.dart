import 'package:flutter/material.dart';
import 'dart:ui' as ui show window;

class RootFrame {
  MediaQueryData? _mediaQueryData;

  static final RootFrame _rootFrame = RootFrame();

  static RootFrame getFrame() {
    _rootFrame._init();
    return _rootFrame;
  }

  _init() {
    MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
    if (_mediaQueryData != mediaQuery) {}
  }

  // 获取宽度
  static double screenWidth(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return mediaQuery.size.width;
  }

  // 获取高度
  static double screenHeight(BuildContext context) =>
      MediaQuery.of(context).size.height;

  // 获取顶部状态拦
  static double screenSafaTop(BuildContext context) =>
      MediaQuery.of(context).padding.top;

  // 获取底部安全区
  static double screenSafaBottom(BuildContext context) =>
      MediaQuery.of(context).padding.bottom;
  // 获取底部安全区
  static double screenNavTopAndStatus(BuildContext context) =>
      MediaQuery.of(context).padding.top+44;
}
