
import 'package:flutter/material.dart';

class RootColors {
  /** 导航条背景颜色*/
  static const nav_back_color = Color.fromRGBO(234, 234, 234, 1);

  /** 导航条字体颜色颜色*/
  static const nav_titile_color = Color.fromRGBO(0, 0, 0, 1);

  /** 公共背景色*/
  static const root_common_bgcolor = Color.fromRGBO(255, 255, 255, 1);

  /** 聊天页面置顶颜色*/
  static const chat_page_top_color = Color.fromRGBO(234, 234, 234, 1);

  /** 聊天页面分割线颜色*/
  static const chat_page_cellline_color = Color.fromRGBO(246, 246, 246, 1);
  /** 聊天页面副标题颜色*/
  static const chat_page_detailtitle_color = Color.fromRGBO(187, 184, 185, 1);
  /** 聊天页面时间颜色*/
  static const chat_page_time_color = Color.fromRGBO(170, 170, 170, 1);

  /** 公共tabbarbei背景色*/
  static const root_tabbar_bgcolor = Color.fromRGBO(245, 246, 247, 1);
}