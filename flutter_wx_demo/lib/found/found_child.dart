import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';

class FoundChild extends StatefulWidget {
  // const FoundChild({Key? key}) : super(key: key);
  String? title;
  FoundChild(this.title);
  @override
  _FoundChildState createState() => _FoundChildState();
}

class _FoundChildState extends State<FoundChild> {
  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        //监听事件等等控制器
        controller: _scrollController,
        //滑动控件是否在头部上面滑过去
        shrinkWrap: false,
        // 阻尼效果
        // physics: BouncingScrollPhysics(),
        physics: ClampingScrollPhysics(),
        slivers: [
          _appBar(),
          _topAdapter(),
          _list(),
        ],
      ),
    );
  }

  Widget _appBar() {
    return SliverAppBar(
      snap: false,
      floating: false,
      pinned: true,
      expandedHeight: 160,
      flexibleSpace: FlexibleSpaceBar(
        title: Container(
          child: Text("${widget.title!}"),
        ),
        background: FlutterLogo(),
        centerTitle: true,
      ),
    );
  }

  Widget _topAdapter() {
    return SliverToBoxAdapter(
      child: Container(
        height: 44,
        child: Text("SliverToBoxAdapter"),
      ),
    );
  }

  Widget _list() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
          return Container(
            height: 44,
            child: Text("第 $index 个"),
          );
        },
        childCount: 100,
      ),
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
}
