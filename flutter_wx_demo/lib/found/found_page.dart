import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';
import 'package:flutter_wx_demo/common/root_custom_indicator.dart';
import 'package:flutter_wx_demo/common/root_tabbar.dart';
import 'package:flutter_wx_demo/found/found_cell.dart';
import 'package:flutter_wx_demo/found/found_model.dart';

class DemoModel {
  final int? id;
  final String? postTitle;
  final List<String>? list;

  DemoModel({this.id, this.postTitle, this.list});

  factory DemoModel.fromMap(Map<String, dynamic> json) =>
      DemoModel(
          id: json["id"],
          postTitle: json["postTitle"],
          list: json["list"] == null ? [] : json["list"]);
}

class Foundpage extends StatefulWidget {
  const Foundpage({Key? key}) : super(key: key);

  @override
  _FoundpageState createState() => _FoundpageState();
}

class _FoundpageState extends State<Foundpage> with TickerProviderStateMixin {
  List<String> _list = ["快讯", "推荐", "订阅"];
  TabController? _tabController;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: _list.length, vsync: this);
    _tabController!.addListener(() {
      print(_tabController!.index);
    });
  }

  Widget _cellForRow(BuildContext context, int index) {
    return FoundCard();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _list.length,
      child: Scaffold(
        appBar: AppBar(
          leadingWidth: 16,
          backgroundColor: RootColors.root_tabbar_bgcolor,
          elevation: 0,
          title: tabLayout(),
        ),
        body: TabBarView(controller: _tabController, children: [
          Container(
            child: Text("data"),
          ),
          Container(
            child: Text("1"),
          ),
          Container(
            child: Text("2"),
          )
        ]),
      ),
    );
  }

  Widget tabLayout() {
    return TabBar(
        controller: _tabController,
        tabs: _list.map((e) {
          return Tab(
            child: Text("${e}"),
          );
        }).toList());
  }
}
