import 'package:flutter/material.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';
import 'package:flutter_wx_demo/found/found_child.dart';
import 'package:flutter_wx_demo/tabbar/root_page.dart';

typedef Click = void Function(String s);
class FoundCell extends StatefulWidget {
  // const FoundCell({Key? key}) : super(key: key);

  final String? t;
  final String? subTitle;
  final String? imgName;
  final String? subImgName;
  final Click? clickBack;
  // Click? clickBack;

  FoundCell({
    Key? key,
    @required this.t,
    this.subTitle,
    @required this.imgName,
    this.subImgName,
    // this.clickBack,
    this.clickBack
  }) : super(key: key);
  // : assert(t != null, 't 不能为空'),
  //       assert(imgName != null, 'imgName不能为空');

 @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FoundCellState();
  }
}
class _FoundCellState extends State<FoundCell> {
  Color _currentColor = RootColors.root_common_bgcolor;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if (widget.clickBack != null) {
          widget.clickBack!(widget.t!);
        }
        setState(() {
          _currentColor = RootColors.root_common_bgcolor;
        });
      },
      onTapDown: (TapDownDetails detail){
        print("爷来了");
        setState(() {
          _currentColor = Colors.grey;
        });
      },
      onTapCancel: (){
        print("爷走了");
        setState(() {
          _currentColor = RootColors.root_common_bgcolor;
        });
      },
      child: Container(
        height: 55,
        color: _currentColor,
        padding: EdgeInsets.only(left: 15, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Row(
                children: [
                  Image.asset(
                    "${widget.imgName}",
                    width: 15,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text("${widget.t}")
                ],
              ),
            ),
            Container(
              child: Row(
                children: [
                  widget.subTitle != null ? Text("${widget.subTitle}") : Text(""),
                  Image.asset(
                    "images/icon_right.png",
                    width: 15,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
