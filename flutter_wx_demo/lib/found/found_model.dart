import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
class FoundCard extends StatelessWidget {
  const FoundCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16,left: 16,right: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            blurRadius: 6,
            spreadRadius: 4,
            color: Color.fromARGB(20, 0, 0, 0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          this.renderCover(),
          this.renderUserInfo(),
          this.renderPublishContent(),
          this.renderInteractionArea(),
        ],
      ),
    );
  }

  Widget renderCover() {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8), topRight: Radius.circular(8)),
          child: Image.asset(
            "images/Hank.png",
            height: 200,
            width: double.infinity,
            fit: BoxFit.fitWidth,
          ),
        ),
        Positioned(
            top: 100,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    Color.fromARGB(0, 0, 0, 0),
                    Color.fromARGB(80, 0, 0, 0),
                  ])),
            ))
      ],
    );
  }

  Widget renderUserInfo() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.symmetric(horizontal: 16),
      color: Colors.red,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(child: Row(
            children: [
              ClipOval(
                child: Image.asset(
                  "images/Hank.png",
                  width: 40,
                  height: 40,
                ),
              ),
              Padding(padding: EdgeInsets.only(left: 8)),
              Container(
                color: Colors.blue,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Hank",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF333333),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 2)),
                    Text(
                      "逻辑教育背后的boss逻辑教育背后的boss逻辑教育背后的boss逻辑教育背后的boss",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 12,
                        color: Color(0xFF999999),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),),
          Text(
            "08:08",
            style: TextStyle(
              fontSize: 13,
              color: Color(0xFF999999),
            ),
          ),
        ],
      ),
    );
  }

  Widget renderPublishContent() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 14),
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
            decoration: BoxDecoration(
                color: Color(0xFFFFC600),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomLeft: Radius.circular(8),
                  bottomRight: Radius.circular(8),
                )),
            child: Text(
              "# ssssss",
              style: TextStyle(
                fontSize: 12,
                color: Colors.white,
              ),
            ),
          ),
          Text(
            "# 根据给的视觉图，我们可以将整体进行拆分，一共划分成4个部分",
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Color(0xFF333333),
            ),
          )
        ],
      ),
    );
  }

  Widget renderInteractionArea() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(
                Icons.messenger,
                size: 16,
                color: Color(0xFF999999),
              ),
              Padding(padding: EdgeInsets.only(left: 6)),
             Text("11",style: TextStyle(
               fontSize: 15,
               color: Color(0xFF999999),
             ),
             )
        ]
          ),
          Row(
              children: [
                Icon(
                  Icons.favorite,
                  size: 16,
                  color: Color(0xFF999999),
                ),
                Padding(padding: EdgeInsets.only(left: 6)),
                Text("11",style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF999999),
                ),
                )
              ]
          ),
          Row(
              children: [
                Icon(
                  Icons.share,
                  size: 16,
                  color: Color(0xFF999999),
                ),
                Padding(padding: EdgeInsets.only(left: 6)),
                Text("11",style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF999999),
                ),
                )
              ]
          ),
        ],
      ),
    );
  }
}

class FoundModel {
  final String? imgUrl;
  final String? userImgUrl;
  final String? userName;
  final String? remark;
  final String? topic;
  final String? publishTime;
  final int? replies;
  final int? likes;
  final int? shares;

  const FoundModel(
      {this.imgUrl,
      this.userImgUrl,
      this.userName,
      this.remark,
      this.topic,
      this.publishTime,
      this.replies,
      this.likes,
      this.shares});
}
