import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_wx_demo/chat/chat_page.dart';
import 'package:flutter_wx_demo/common/root_colors.dart';
import 'package:flutter_wx_demo/found/found_page.dart';
import 'package:flutter_wx_demo/friends/friends_page.dart';
import 'package:flutter_wx_demo/mine/mine_page.dart';
import 'package:flutter_wx_demo/tabbar/root_model.dart';

class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _curttindex = 0;
  List<Widget> _pages = [ChatPage(), FriendsPage(), Foundpage(), MinePage()];
  PageController _controller = PageController(
      initialPage: 0, keepPage: true, viewportFraction: 1);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        bottomNavigationBar: _bottombar(),
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _controller,
          children: _pages,
          onPageChanged: (int index) {
            setState(() {
              _curttindex = index;
            });
          },
        ),
      ),
    );
  }

  List<Widget> _navActions() {
    return [
      TextButton(
          onPressed: () {},
          child: Container(
            height: 44,
            child: Row(
              children: [
                Image.asset(
                  "images/放大镜b.png",
                  width: 44,
                  height: 20,
                ),
                Image.asset(
                  "images/圆加.png",
                  width: 44,
                  height: 20,
                  color: Colors.black,
                )
              ],
            ),
          ))
      // TextButton(
      //     onPressed: () {},
      //     child: Image.asset(
      //       "images/圆加.png",
      //       width: 20,
      //       height: 20,
      //     ))
    ];
  }

  /// tabbar
  Widget _bottombar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      fixedColor: Colors.green,
      selectedFontSize: 12.0,
      currentIndex: _curttindex,
      // selectedLabelStyle: const TextStyle(
      //   fontWeight: FontWeight.bold,
      // ),
      onTap: (index) {
        setState(() {
          _curttindex = index;
          _controller.jumpToPage(_curttindex);
        });
      },
      items: [
        _bottombarItem(tabbarList[0]),
        _bottombarItem(tabbarList[1]),
        _bottombarItem(tabbarList[2]),
        _bottombarItem(tabbarList[3]),
      ],
    );
  }

  /// tabbarItem
  BottomNavigationBarItem _bottombarItem(item) {
    return BottomNavigationBarItem(
      icon: Image.asset(
        item.img,
        width: 20,
        height: 20,
      ),
      activeIcon: Image.asset(
        item.selImg,
        width: 20,
        height: 20,
      ),
      label: item.title,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
}
