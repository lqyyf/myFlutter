

class RootModel {
  final String? title;
  final String? img;
  final String? selImg;
 const RootModel(this.title,this.img,this.selImg);
}
final List<RootModel> tabbarList = [
  const RootModel("微信", "images/tabbar_chat.png", "images/tabbar_chat_hl.png"),
  const RootModel("通讯录", "images/tabbar_friends.png", "images/tabbar_friends_hl.png"),
  const RootModel("发信", "images/tabbar_discover.png", "images/tabbar_discover_hl.png"),
  const RootModel("我的", "images/tabbar_mine.png", "images/tabbar_mine_hl.png"),
];